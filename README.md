# DogsVSCats-Classification-with-KNNs


Install All Libraries Essential: You can use virtualenv to create virtual enviroment.

    virtualenv env
    . env/bin/active #active env
    pip install -r requiemnets.txt
    


Step 1: Init data: -Save path all image to file txt.
                    - Split data (70% train, 30% test).

    python Initdata.py

Step 2: Extract Feature: Use VGG16 fc2 to extract features of image and save them in Features folder.

    python ExtractFeatures.py

Step 3: Classification: USe KNNs to classify.

    python Classification.py 

Step 4: Analyze results: Use 30% image to analyze result with Accurancy Score.

    python Analyze.py


RESULT: 


![](./temp/Screenshot_from_2018-10-21_11-12-03.png)