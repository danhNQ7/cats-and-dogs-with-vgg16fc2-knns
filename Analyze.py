import numpy as np 
from sklearn.cluster import KMeans
import pickle
import os

from sklearn.metrics import accuracy_score
PATH = 'Path/test30.txt'
filename = 'ModelKNNs.sav'
SAVEFOLDER = 'Features'
testX = []
TargetTestY =[]
#prepare data:
with open(PATH,'r') as f:
    pathImages = f.read().strip('\n').split('\n')
for path in pathImages:
    temp = path.rfind('/')
    pathFeature = '{}/{}.npy'.format(SAVEFOLDER,path[path[0:temp].rfind('/')+1:path.rfind('.')])
    testX.append(np.load(pathFeature)[0])
    nameFolder = path[path.find('/')+1:path.find('.')]
    TargetTestY.append(nameFolder)
#Load model
with open(filename, 'rb') as file:  
    clf = pickle.load(file)
trainY = list(clf.predict(np.array(testX)))
#Accurancy
print('Accurancy: {}'.format(accuracy_score(TargetTestY,trainY)))
