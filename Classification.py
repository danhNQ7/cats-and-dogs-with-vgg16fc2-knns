import numpy as np 
from sklearn.cluster import KMeans
from sklearn.svm import LinearSVC
import pickle
import os
from sklearn.neighbors import KNeighborsClassifier
import time
#load data
NCLUSTERS = 5
PATH = 'Path/train70.txt'
filename = 'ModelKNNs.sav'

with open(PATH,'r') as f:
    pathImages = f.read().strip('\n').split('\n')
dataX = []
limit = 0
trainY=[]
for path in pathImages:
    tag = path[path.find('/')+1:path.find('.')]
    print(tag)
    # input()
    trainY.append(tag)
if not os.path.exists(filename):
    for path in pathImages:
        temp = path.rfind('/')
        pathFeature = 'Features/{}.npy'.format(path[path[0:temp].rfind('/')+1:path.rfind('.')])
        dataX.append(np.load(pathFeature)[0])
    print('Prepare Data: Done!')
    begin = time.time()

    # clf = KMeans(n_clusters=NCLUSTERS,random_state=0)
    # clf = LinearSVC(random_state=0, tol=1e-5,C=1000)
    clf = KNeighborsClassifier(n_neighbors=3)
    clf.fit(dataX,trainY)
    pickle.dump(clf, open(filename, 'wb'))
    # np.save('logLabel',clf.labels_)
else:
    with open(filename, 'rb') as file:  
        clf = pickle.load(file)
print("Time {}".format(time.time()-begin))


